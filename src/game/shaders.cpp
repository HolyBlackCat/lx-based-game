#include "shaders.h"

namespace Shaders
{
    #define SHADER(name, ...) Graphics::Shader *name;
    SHADER_LIST
    #undef SHADER

    namespace Source
    {
        #if OnPC(1) OnMobile(0)
        #define VERTEX_PREFIX "#version 330"
        #define FRAGMENT_PREFIX "#version 330"
        #else
        #define VERTEX_PREFIX "#version 100"
        #define FRAGMENT_PREFIX "#version 100\nprecision mediump float;"
        #endif

        namespace main
        {
            Graphics::ShaderSource txt{
VERTEX_PREFIX R"(
uniform mat2 u_matrix;
attribute vec2 a_pos;
attribute vec4 a_color;
attribute vec2 a_tex;
attribute vec2 a_fac;
attribute vec4 a_cadd;
attribute vec4 a_cmul;
varying vec4 v_color;
varying vec2 v_tex;
varying vec2 v_fac;
varying vec4 v_cadd;
varying vec4 v_cmul;
void main()
{
    gl_Position = vec4(u_matrix * a_pos, 0, 1);
    v_color = a_color;
    v_tex = a_tex / 1024;
    v_fac = a_fac;
    v_cadd = a_cadd;
    v_cmul = a_cmul;
}
)",
FRAGMENT_PREFIX R"(
uniform sampler2D u_texture;
varying vec4 v_color;
varying vec2 v_tex;
varying vec2 v_fac;
varying vec4 v_cadd;
varying vec4 v_cmul;
void main()
{
    gl_FragColor = texture2D(u_texture, v_tex) * v_fac.xxxy + v_color * (1-v_fac).xxxy;
    gl_FragColor = gl_FragColor * v_cmul + v_cadd;
    gl_FragColor.rgb *= gl_FragColor.a;
}
)"};
        }

        namespace post
        {
            Graphics::ShaderSource txt{
VERTEX_PREFIX R"(
attribute vec2 a_pos;
void main()
{
    gl_Position = vec4(a_pos, 0, 1);
}
)",
FRAGMENT_PREFIX R"(
uniform sampler2D u_texture;
uniform vec2 u_fac;
uniform vec2 u_screensz;
void main()
{
    gl_FragColor = texture2D(u_texture, gl_FragCoord.xy * u_fac);
    vec2 pos = gl_FragCoord.xy / u_screensz;
    float factor = 1-(1-clamp(pos.x*pos.y*(1-pos.x)*(1-pos.y)/0.0625,0,1))/5;
    gl_FragColor.rgb *= factor;
    gl_FragColor.a *= 1-factor;
}
)"};
        }
    }

    void Init()
    {
        ExecuteThisOnce();
        #define SHADER(name, ...) name = new Graphics::Shader(#name, Source::name::txt, __VA_ARGS__);
        SHADER_LIST
        #undef SHADER
    }
}