#include "sounds.h"

#include <random>

#include "random.h"

namespace Sounds
{
    #define SOUND(name) static Audio::Buffer *buf_##name;
    SOUND_LIST
    #undef SOUND

    static constexpr float variance = 0.25;

    #define SOUND(name) void name(           float vol, float pitch) {buf_##name->Play(               vol, pitch);} \
                        void name(ivec2 pos, float vol, float pitch) {buf_##name->Play(pos.to_vec3(), vol, pitch);} \
                        void name##_r(           float vol) {buf_##name->Play(               vol, std::pow(2, Random::FloatPlusMinus1() * variance));} \
                        void name##_r(ivec2 pos, float vol) {buf_##name->Play(pos.to_vec3(), vol, std::pow(2, Random::FloatPlusMinus1() * variance));}
    SOUND_LIST
    #undef SOUND

    void Init()
    {
        ExecuteThisOnce();

        #define SOUND(name) buf_##name = new Audio::Buffer(Audio::SoundData::FromWAV("assets/" #name ".wav"));
        SOUND_LIST
        #undef SOUND
    }
}