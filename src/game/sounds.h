#ifndef SOUNDS_H_INCL
#define SOUNDS_H_INCL

#include "master.h"

namespace Sounds
{
    #define SOUND_LIST \
        SOUND(click) \

    #define SOUND(name) void name(           float vol = 1, float pitch = 1); \
                        void name(ivec2 pos, float vol = 1, float pitch = 1); \
                        void name##_r(           float vol = 1); \
                        void name##_r(ivec2 pos, float vol = 1);
    SOUND_LIST
    #undef SOUND

    void Init();
}

#endif