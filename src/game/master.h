#ifndef MASTER_H_INCLUDED
#define MASTER_H_INCLUDED

#include "../lib_sdl.h"
#include "../lib_sdlnet.h"
#include "../lib_gl.h"
#include "../lib_al.h"
#include "../lib_zlib.h"
#include "../lib_ogg.h"
#include "../lib_vorbis.h"
#include "../audio.h"
#include "../exceptions.h"
#include "../graphics.h"
#include "../input.h"
#include "../input_enums.h"
#include "../math.h"
#include "../network.h"
#include "../system.h"
#include "../utils.h"
#include "../window.h"

#include <algorithm>
#include <functional>

namespace Master
{
    extern Graphics::ImageData *main_tex_data;

    ivec2 WinSz();
    ivec2 WinSzHalf();

    namespace Layouts
    {
        //                              pos    color  tex    fac    c_add  c_mul
        using Main = Graphics::TypePack<fvec2, fvec4, fvec2, fvec2, fvec4, fvec4>;
        using Post = Graphics::TypePack<fvec2>;
    }

    extern Graphics::Texture *main_tex;
    extern Graphics::RenderingQueue<Layouts::Main> *ren_que;

    void Loop(std::function<void()> tick, std::function<void()> render, std::function<bool()> post = 0); // If post() returns 1, then the Loop() itself returns.
}

#endif