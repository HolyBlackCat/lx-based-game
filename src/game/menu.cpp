#include "menu.h"
#include "strings.h"
#include "sounds.h"

namespace Scenes
{
    void Menu()
    {
        static constexpr int button_count = 2;
        const char *const button_names[button_count]{Strings::menu_singleplayer(), Strings::menu_quit()}; // This is not static because locale may change during execution.
        static constexpr void (*button_functions[button_count])(){Sys::Exit, Sys::Exit};

        static constexpr int menu_y_offset = 32;
        const ivec2 button_hitbox(192,28);

        static constexpr ivec2 button_text_shadow_offset(1,1);
        static constexpr fvec4 button_text_main_color    = fvec4(191,191,191,255)/255,
                               button_text_shadow_color  = fvec4(45,45,45,255)/255,
                               button_text_active_color  = fvec4(255,255,255,255)/255,
                               button_text_pressed_color = fvec4(224,224,224,255)/255,
                               marker_main_color         = fvec4(255,191,0,255)/255,
                               marker_shadow_color       = button_text_shadow_color;
        static constexpr int marker_max_offset = 16;

        static constexpr float button_anim_step = 0.04;

        float dimming = 1;
        void (*exit_to)() = 0;

        int selection;
        float button_states[button_count]{};

        auto Tick = [&]()
        {
            // Update selection
            selection = -1;
            for (int i = 0; i < button_count; i++)
            {
                if (Input::MouseInRect({-button_hitbox.x/2, menu_y_offset+button_hitbox.y*i - button_hitbox.y/2}, button_hitbox))
                {
                    selection = i;
                    break;
                }
            }

            // Update button animations
            for (int i = 0; i < button_count; i++)
            {
                if (i == selection)
                {
                    if (button_states[i] < 1)
                    {
                        button_states[i] += button_anim_step;
                        if (button_states[i] > 1)
                            button_states[i] = 1;
                    }
                }
                else
                {
                    if (button_states[i] > 0)
                    {
                        button_states[i] -= button_anim_step;
                        if (button_states[i] < 0)
                            button_states[i] = 0;
                    }
                }
            }

            // Click sound
            if (Input::MouseButtonPressed(1) && selection != -1)
            {
                Sounds::click_r();
                exit_to = button_functions[selection];
            }

            // Fade
            Draw::UpdateDimBetweenScenes(dimming, exit_to);
        };
        auto Render = [&]()
        {
            glClear(GL_COLOR_BUFFER_BIT);

            Draw::ColorRange();
            Draw::RectFullscreenTiled({624,0},{400,300}); // Sky

            for (int i = 0; i < button_count; i++)
            {
                for (bool shadow : {1,0})
                {
                    int vislen = Draw::GetVisualStringLenLarge(button_names[i]);
                    int off = smoothstep((1-button_states[i])/2)*marker_max_offset;
                    float alpha = std::pow(button_states[i], 2);
                    bool pressed = Input::MouseButtonDown(1) && selection == i;
                    ivec2 pos = ivec2(0,menu_y_offset+button_hitbox.y*i-Draw::FontLarge::tex_h/2);
                    fvec4 txtcolor;
                    if (shadow)
                        txtcolor = button_text_shadow_color;
                    else if (pressed)
                        txtcolor = button_text_pressed_color;
                    else
                        txtcolor = (selection == i ? button_text_active_color : button_text_main_color);
                    Draw::TextLarge(button_names[i], pos+button_text_shadow_offset*shadow+ivec2(0,pressed), txtcolor, Draw::center);
                    Draw::TextLarge("� ", pos-ivec2(vislen/2+off,0)+button_text_shadow_offset*shadow, (shadow ? marker_shadow_color : marker_main_color).change_a(alpha), Draw::right);
                    Draw::TextLarge(" �", pos+ivec2(vislen/2+off,0)+button_text_shadow_offset*shadow, (shadow ? marker_shadow_color : marker_main_color).change_a(alpha), Draw::left);
                }
            }

            Draw::ColorRange();
            Draw::Mouse();
            Draw::Dim(dimming);
        };
        auto Post = [&]() -> bool
        {
            if (exit_to && dimming >= 1)
            {
                Sys::SetCurrentFunction(exit_to);
                return 1;
            }
            return 0;
        };

        Master::Loop(Tick, Render, Post);
    }
}