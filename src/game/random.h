#ifndef RANDOM_H_INCL
#define RANDOM_H_INCL

#include "master.h"

#include <random>

namespace Random
{
    std::mt19937 &Generator()
    {
        static std::mt19937 ret(Utils::Clock::Time());
        return ret;
    }

    int Integer(int lim) // Range: [0 ; lim)
    {
        return Generator()() % lim;
    }

    int Integer(int min, int max) // Range: [min ; max]
    {
        return Generator()() % (max - min + 1);
    }

    float Float01(int precision = 10000) // Range: [0 ; 1]
    {
        return Integer(precision+1) / float(precision);
    }

    float FloatPlusMinus1(int precision = 10000) // Range: [-1 ; 1]
    {
        return (Integer(precision*2+1) - precision) / float(precision);
    }
}

#endif