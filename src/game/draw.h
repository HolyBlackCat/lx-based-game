#ifndef DRAW_H_INCL
#define DRAW_H_INCL

#include "master.h"

namespace Draw
{
    fmat2x4 GetColorSettings();
    void ColorRange();
    void ColorRange(fvec4 from, fvec4 to);
    void ColorRange(fvec2 from, fvec2 to);
    void ColorRange(float from, float to);

    inline void Rect(ivec2 dst, ivec2 dstsz, ivec2 src, ivec2 srcsz, fvec4 color, fvec2 texfac, bool flip = 0)
    {
        dstsz += dst;
        srcsz += src;
        if (flip) std::swap(dst.x, dstsz.x);
        Master::ren_que->Push4as3x2({fvec2(dst.x,   dst.y  ), color, fvec2(src.x,   src.y  ), texfac, GetColorSettings().x, GetColorSettings().y},
                                    {fvec2(dstsz.x, dst.y  ), color, fvec2(srcsz.x, src.y  ), texfac, GetColorSettings().x, GetColorSettings().y},
                                    {fvec2(dstsz.x, dstsz.y), color, fvec2(srcsz.x, srcsz.y), texfac, GetColorSettings().x, GetColorSettings().y},
                                    {fvec2(dst.x,   dstsz.y), color, fvec2(src.x,   srcsz.y), texfac, GetColorSettings().x, GetColorSettings().y});
    }

    inline void Rect(ivec2 dst, ivec2 dstsz, ivec2 src, fvec4 color, fvec2 texfac, bool flip = 0)
    {
        Rect(dst, dstsz, src, dstsz, color, texfac, flip);
    }

    inline void Rect(ivec2 dst, ivec2 dstsz, ivec2 src, fvec4 color, bool flip = 0)
    {
        Rect(dst, dstsz, src, color.change_a(0), {0,color.a}, flip);
    }

    inline void Rect(ivec2 dst, ivec2 dstsz, ivec2 src, bool flip = 0)
    {
        Rect(dst, dstsz, src, dstsz, 0, 1, flip);
    }

    inline void RectFill(ivec2 dst, ivec2 dstsz, fvec4 color)
    {
        Rect(dst, dstsz, 0, 0, color, 0, 0);
    }

    enum Align {left, center, right};

    template <unsigned int X, unsigned int Y, unsigned int W, unsigned int H, unsigned int MaxExtraPixels, unsigned int MaxExtraPixelsCap, unsigned int MaxExtraColumns, unsigned int MaxExtraColumnsCap, int Gap, int SpaceLen>
    struct GenericFont
    {
        static constexpr unsigned int tex_x = X, tex_y = Y, tex_w = W, tex_h = H;
        static constexpr unsigned int max_extra_pixels      = MaxExtraPixels;
        static constexpr unsigned int max_extra_pixels_cap  = MaxExtraPixelsCap;
        static constexpr unsigned int max_extra_columns     = MaxExtraColumns;
        static constexpr unsigned int max_extra_columns_cap = MaxExtraColumnsCap;
        static constexpr int gap = Gap, space_len = SpaceLen;

        struct FontInfo
        {
            int tex_off_arr[256], l_off_arr[256], r_off_arr[256], sz_arr[256];
        };

        static const FontInfo &Info()
        {
            static FontInfo data;
            static bool first = 1;
            if (first)
            {
                first = 0;
                auto CheckLine = [&](ivec2 start, int len) -> int
                {
                    int ret = 0;
                    while (len--)
                    {
                        if (Master::main_tex_data->At(start).a != 0)
                            ret++;
                        start.y++;
                    }
                    return ret;
                };

                for (int y = 0; y < 16; y++)
                {
                    for (int x = 0; x < 16; x++)
                    {
                        int &cur_t_off = data.tex_off_arr[x+y*16] = 0;
                        int &cur_l_off = data.l_off_arr  [x+y*16] = 0;
                        int &cur_r_off = data.r_off_arr  [x+y*16] = 0;
                        int &cur_sz    = data.sz_arr     [x+y*16] = tex_w;
                        int extra_pixels;
                        int extra_columns;
                        int tmp;
                        if ((char(x+y*16) >= 'A' && char(x+y*16) <= 'Z') || (char(x+y*16) >= '�' && char(x+y*16) <= '�'))
                        {
                            extra_pixels = max_extra_pixels_cap;
                            extra_columns = max_extra_columns_cap;
                        }
                        else
                        {
                            extra_pixels = max_extra_pixels;
                            extra_columns = max_extra_columns;
                        }
                        for (unsigned int i = 0; i < tex_w; i++)
                        {
                            tmp = CheckLine(ivec2(tex_x+x*tex_w+i,tex_y+y*tex_h), tex_h);
                            if (tmp > 0)
                            {
                                if (tmp <= extra_pixels && extra_columns)
                                {
                                    cur_l_off--;
                                    extra_pixels -= tmp;
                                    extra_columns--;
                                }
                                else
                                    break;
                            }
                            else
                            {
                                cur_t_off++;
                                cur_sz--;
                            }
                        }
                        if (cur_sz == 0)
                        {
                            cur_t_off = 0;
                            cur_l_off = 0;
                            cur_sz = space_len;
                            continue;
                        }
                        if ((char(x+y*16) >= 'A' && char(x+y*16) <= 'Z') || (char(x+y*16) >= '�' && char(x+y*16) <= '�'))
                        {
                            extra_pixels = max_extra_pixels_cap;
                            extra_columns = max_extra_columns_cap;
                        }
                        else
                        {
                            extra_pixels = max_extra_pixels;
                            extra_columns = max_extra_columns;
                        }
                        for (unsigned int i = tex_w-1; i >= 0; i--)
                        {
                            tmp = CheckLine(ivec2(tex_x+x*tex_w+i,tex_y+y*tex_h), tex_h);
                            if (tmp > 0)
                            {
                                if (tmp <= extra_pixels && extra_columns)
                                {
                                    cur_r_off--;
                                    extra_pixels -= tmp;
                                    extra_columns--;
                                }
                                else
                                    break;
                            }
                            else
                                cur_sz--;
                        }
                    }
                }
            }
            return data;
        }
    };
    using FontSmall = GenericFont<0,0,6,12,0,0,0,0,1,3>;
    using FontLarge = GenericFont<0,192,12,21,0,2,0,1,2,5>;

    template <typename Font>
    int GenericGetVisualStringLenMono(const char *ptr) // Breaks at first new-line or at null-terminator.
    {
        int ret = 0;
        while (*ptr != 0 && *ptr != '\n')
        {
            ret += Font::tex_w;
            ptr++;
        }
        return ret;
    }

    template <typename Font>
    void GenericTextMono(const char *txt, ivec2 pos, fvec4 color, Align align = left)
    {
        ivec2 curpos = pos;
        curpos.x -= GenericGetVisualStringLenMono<Font>(txt) * align / 2;
        while (*txt)
        {
            if (*txt != '\n')
            {
                Draw::Rect(curpos, ivec2(Font::tex_w,Font::tex_h), ivec2((unsigned char)*txt % 16 * Font::tex_w + Font::tex_x, (unsigned char)*txt / 16 * Font::tex_h + Font::tex_y), color);
                curpos.x += Font::tex_w;
            }
            else
            {
                curpos.x = pos.x - GenericGetVisualStringLenMono<Font>(txt+1) * align / 2;
                curpos.y += Font::tex_h;
            }
            txt++;
        }
    }

    template <typename Font>
    auto GenericGetVisualStringLen(const char *ptr) // Breaks at first new-line or at null-terminator.
    {
        int ret = -Font::gap; // We don't want to count extra gap on the right side.
        while (*ptr != 0 && *ptr != '\n')
        {
            ret += Font::Info().sz_arr[(unsigned char)*ptr] + Font::gap + Font::Info().l_off_arr[(unsigned char)*ptr] + Font::Info().r_off_arr[(unsigned char)*ptr];
            ptr++;
        }
        return ret;
    }

    template <typename Font>
    void GenericText(const char *txt, ivec2 pos, fvec4 color, Align align = left)
    {
        ivec2 curpos = pos;
        curpos.x -= GenericGetVisualStringLen<Font>(txt) * align / 2;
        while (*txt)
        {
            if (*txt != '\n')
            {
                Draw::Rect(curpos + ivec2(Font::Info().l_off_arr[(unsigned char)*txt], 0),
                           ivec2(Font::Info().sz_arr[(unsigned char)*txt],Font::tex_h),
                           ivec2((unsigned char)*txt % 16 * Font::tex_w + Font::tex_x + Font::Info().tex_off_arr[(unsigned char)*txt], (unsigned char)*txt / 16 * Font::tex_h + Font::tex_y), color);
                curpos.x += Font::Info().sz_arr[(unsigned char)*txt] + Font::gap + Font::Info().l_off_arr[(unsigned char)*txt] + Font::Info().r_off_arr[(unsigned char)*txt];
            }
            else
            {
                curpos.x = pos.x - GenericGetVisualStringLen<Font>(txt+1) * align / 2;
                curpos.y += Font::tex_h;
            }
            txt++;
        }
    }

    inline int GetVisualStringLenSmallMono(const char *ptr)
    {
        return GenericGetVisualStringLenMono<FontSmall>(ptr);
    }
    inline int GetVisualStringLenSmall(const char *ptr)
    {
        return GenericGetVisualStringLen<FontSmall>(ptr);
    }
    inline void TextSmallMono(const char *txt, ivec2 pos, fvec4 color, Align align = Align::left)
    {
        GenericTextMono<FontSmall>(txt, pos, color, align);
    }
    inline void TextSmall(const char *txt, ivec2 pos, fvec4 color, Align align = Align::left)
    {
        GenericText<FontSmall>(txt, pos, color, align);
    }

    inline int GetVisualStringLenLargeMono(const char *ptr)
    {
        return GenericGetVisualStringLenMono<FontLarge>(ptr);
    }
    inline int GetVisualStringLenLarge(const char *ptr)
    {
        return GenericGetVisualStringLen<FontLarge>(ptr);
    }
    inline void TextLargeMono(const char *txt, ivec2 pos, fvec4 color, Align align = Align::left)
    {
        GenericTextMono<FontLarge>(txt, pos, color, align);
    }
    inline void TextLarge(const char *txt, ivec2 pos, fvec4 color, Align align = Align::left)
    {
        GenericText<FontLarge>(txt, pos, color, align);
    }

    inline void Mouse()
    {
        OnMobile(return;)
        if (!Input::MouseFocus())
            return;
        Draw::Rect(Input::MousePos(), 16, {96,0});
    }

    inline void RectFullscreenTiled(ivec2 src, ivec2 srcsz)
    {
        ivec2 size = (Master::WinSzHalf() - srcsz/2 + srcsz - 1) / srcsz;
        ivec2 start = -srcsz/2 - srcsz * size;
        size = size * 2 + 1;
        for (int y = 0; y < size.y; y++)
            for (int x = 0; x < size.x; x++)
                Draw::Rect(start + srcsz*ivec2(x,y), srcsz, src);
    }

    inline void UpdateDimBetweenScenes(float &var, bool exiting)
    {
        static constexpr float step = 0.03;
        if (exiting)
        {
            if (var < 1)
            {
                var += step;
                if (var > 1)
                    var = 1;
            }
        }
        else
        {
            if (var > 0)
            {
                var -= step;
                if (var < 0)
                    var = 0;
            }
        }
    }
    inline void Dim(float alpha, fvec3 color = {0,0,0})
    {
        if (alpha == 0)
            return;
        Draw::RectFill(-Master::WinSzHalf(), Master::WinSz(), color.to_vec4().change_a(alpha));
    }
}

#endif