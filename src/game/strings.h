#ifndef STRINGS_H_INCL
#define STRINGS_H_INCL

class Strings
{
    #define STRINGS \
        STR( menu_singleplayer , "Singleplayer" ) \
        STR( menu_quit         , "Quit"         ) \

    ~Strings();
  public:
    #define STR(name, text) static const char *name() {return text;}
    STRINGS
    #undef STR
    #undef STRINGS
};

#endif