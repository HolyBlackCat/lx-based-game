#ifndef SHADERS_H_INCL
#define SHADERS_H_INCL

#include "master.h"

namespace Shaders
{
    #define SHADER_LIST \
        SHADER(main, {"a_pos","a_color","a_tex","a_fac","a_cadd","a_cmul"}, {"u_texture","u_matrix"}) \
        SHADER(post, {"a_pos"}, {"u_texture","u_fac","u_screensz"}) \

    #define SHADER(name, ...) extern Graphics::Shader *name;
    SHADER_LIST
    #undef SHADER

    void Init();
}

#endif