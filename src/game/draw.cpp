#include "draw.h"

namespace Draw
{
    fmat2x4 color_settings(0,0,0,0,1,1,1,1);

    fmat2x4 GetColorSettings()
    {
        return color_settings;
    }
    void ColorRange()
    {
        color_settings.x = {0,0,0,0};
        color_settings.y = {1,1,1,1};
    }
    void ColorRange(fvec4 from, fvec4 to)
    {
        color_settings.x = from;
        color_settings.y = to - from;
    }
    void ColorRange(fvec2 from, fvec2 to)
    {
        ColorRange({from.x,from.x,from.x,from.y},{to.x,to.x,to.x,to.y});
    }
    void ColorRange(float from, float to)
    {
        ColorRange({from,0},{to,1});
    }
}