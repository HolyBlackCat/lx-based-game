#include "master.h"

#include "draw.h"
#include "menu.h"
#include "shaders.h"
#include "sounds.h"

namespace Master
{
    void Boot();
    void Menu();

    Graphics::ImageData *main_tex_data;
    Graphics::Texture *main_tex;
    Graphics::RenderingQueue<Layouts::Main> *ren_que;
    Graphics::Framebuffer *fbuffer;
    Graphics::Texture *fbuffer_tex;
    Graphics::SizedVertexArray<Layouts::Post> *fbuffer_tri;

    static constexpr ivec2 ref_screen_size(OnPC(400,300) OnMobile(320,240));

    ivec2 window_sz;
    int scale;
    ivec2 WinSz() {return window_sz;}
    ivec2 WinSzHalf() {return window_sz/2;}

    void Loop(std::function<void()> tick, std::function<void()> render, std::function<bool()> post)
    {
        Utils::TickStabilizer ts(60, 8);
        while (1)
        {
            Sys::BeginFrame();
            while (ts.Tick())
            {
                Sys::Tick();
                tick();
            }
            Graphics::Blend::Enable();
            Shaders::main->Use();
            fbuffer->BindDraw();
            Graphics::Viewport(WinSz());
            ren_que->Clear();
            render();
            ren_que->Send();
            ren_que->DrawTriangles();
            Graphics::Blend::Disable();
            Shaders::post->Use();
            Graphics::Framebuffer::UnbindDraw();
            Graphics::Viewport((Window::Size() - window_sz*scale)/2, window_sz * scale);
            fbuffer_tri->DrawTriangles();
            Sys::EndFrame();
            if (post && post()) return;
        }
    }

    void PreInit()
    {
        Sys::SetCurrentFunction(Boot);
        Sys::Config::window_min_size = OnPC(ref_screen_size*2) OnMobile(ivec2(0,0));
    }

    void Resize()
    {
        // Update scale and logical window size
        window_sz = (Window::Size() + 1) / 2;
        scale = std::min(window_sz.x/(ref_screen_size.x/2),window_sz.y/(ref_screen_size.y/2));
        window_sz = (window_sz + scale - 1) / scale * 2;

        // Update main shader matrix
        Shaders::main->SetUniform<fmat2>(1, fmat2::ortho2D(window_sz*ivec2(1,-1)));

        // Update framebuffer texture size
        fbuffer_tex->SetData(window_sz);

        // Update screen size for post shader
        Shaders::post->SetUniform<fvec2>(1, 1.f / window_sz / scale);
        Shaders::post->SetUniform<fvec2>(2, window_sz * scale);
    }

    void Boot()
    {
        MarkLocation("Boot");
        ExecuteThisOnce();

        Input::ShowMouseImmediate(0);
        Input::SetMouseMapping(
        [&](ivec2 in) -> ivec2
        {
            return in / scale - window_sz/2;
        },
        [&](ivec2 in) -> ivec2
        {
            return (in + window_sz/2) * scale;
        });

        Graphics::Blend::Presets::FuncNormal_Pre();

        fbuffer_tri = new Graphics::SizedVertexArray<Layouts::Post>({{{0,-3}},{{-3,2}},{{3,2}}});

        ren_que = new Graphics::RenderingQueue<Layouts::Main>("Main", 20000*3);

        fbuffer_tex = new Graphics::Texture(ref_screen_size);
        fbuffer_tex->LinearInterpolation(0);

        main_tex_data = new Graphics::ImageData;
        main_tex_data->LoadTGA("assets/texture.tga", Graphics::Mirror::y);
        main_tex = new Graphics::Texture(*main_tex_data);
        main_tex->LinearInterpolation(0);

        fbuffer = new Graphics::Framebuffer;
        fbuffer->BindDrawAndAttachTexture(*fbuffer_tex);

        Shaders::Init();
        Shaders::main->SetUniform<Graphics::Texture>(0, *main_tex);
        Shaders::post->SetUniform<Graphics::Texture>(0, *fbuffer_tex);

        Sounds::Init();

        Sys::SetCurrentFunction(Scenes::Menu);
    }
}