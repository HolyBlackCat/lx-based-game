#ifndef OS_H_INCLUDED
#define OS_H_INCLUDED

#if defined(ANDROID) || defined(__ANDROID__)
#define LXINTERNAL_OS_ANDROID
#define LXINTERNAL_OS_TYPE_MOBILE
#elif defined(_WIN32) || defined(__WINDOWS__)
#define LXINTERNAL_OS_WINDOWS
#define LXINTERNAL_OS_TYPE_PC
#elif defined(__APPLE__) && defined(__MACH__)
#define LXINTERNAL_OS_MAC
#define LXINTERNAL_OS_TYPE_PC
#else // Default OS.
#define LXINTERNAL_OS_WINDOWS
#define LXINTERNAL_OS_TYPE_PC
#endif


#if defined(LXINTERNAL_OS_WINDOWS)
#define OnPC(...) __VA_ARGS__
#define OnWindows(...) __VA_ARGS__
#define OnMac(...)
#define OnMobile(...)
#elif defined(LXINTERNAL_OS_MAC)
#define OnPC(...) __VA_ARGS__
#define OnWindows(...)
#define OnMac(...) __VA_ARGS__
#define OnMobile(...)
#elif defined(LXINTERNAL_OS_ANDROID)
#define OnPC(...)
#define OnWindows(...)
#define OnMac(...)
#define OnMobile(...) __VA_ARGS__
#else
#error No OS specified.
#endif


#define IgnoredOnMobile OnMobile([[deprecated("Does not work on OpenGL 2 ES, ignored.")]])
#define ReplacedOnMobile OnMobile([[deprecated("Does not work on OpenGL 2 ES, replaced.")]])


#endif