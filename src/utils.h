#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <array>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#include <type_traits>

#include "lib_sdl.h"
#include "system.h"

#define ExecuteThisOnce() do {static bool flag = 0; if (flag) ::Sys::Error(Jo("At function ", __func__, ": Statement at " __FILE__ ":", __LINE__, " was illegally executed twice.")); flag = 1;} while (0)
#define ExecuteThisOnceQuiet() do {static bool flag = 0; if (flag) ::Sys::Exit(); flag = 1;} while (0)
#define Textify(...) #__VA_ARGS__

namespace Utils
{
    inline namespace Templates
    {
        template <typename T> struct DisableDeduction_Internal {using type = T;};
        template <typename T> using DisableDeduction = typename DisableDeduction_Internal<T>::type;

        template <unsigned int X, typename T, typename ...P> struct TypeAt {using type = typename TypeAt<X-1, P...>::type;};
        template <typename T, typename ...P> struct TypeAt<0, T, P...> {using type = T;};
    }

    namespace Clock
    {
        inline uint64_t Time()                         {return SDL_GetPerformanceCounter();}
        inline uint64_t Tps()                          {static uint64_t ret = SDL_GetPerformanceFrequency(); return ret;}
        inline uint64_t Tpms()                         {static uint64_t ret = Tps()/1000; return ret;}
        inline uint64_t SecsToTicks(long double secs)  {return secs * Tps();}
        inline long double TicksToSecs(uint64_t units) {return (long double)units / (long double)Tps();}

        inline void WaitTicks(uint64_t delay)
        {
            uint64_t begin = Time();
            while (Time() - begin < delay) {}
        }
        inline void WaitSecs(long double secs)
        {
            WaitTicks(SecsToTicks(secs));
        }
    }

    namespace SysInfo
    {
        static constexpr bool is_char_signed = char(-1) < 0;
        static constexpr bool big_endian = SDL_BYTEORDER == SDL_BIG_ENDIAN;
    }

    namespace Strings
    {
        namespace Internal
        {
            extern std::stringstream ss;
            extern const std::stringstream::fmtflags stdfmt;
        }
        template <typename ...P> std::string Jo_s(P &&... p)
        {
            Internal::ss.clear();
            Internal::ss.flags(Internal::stdfmt);
            Internal::ss.str("");
            int dummy[] {(Internal::ss << p, 0)...};
            (void)dummy;
            return Internal::ss.str();
        }
        template <typename ...P> const char *Jo(P &&... p)
        {
            static constexpr int ret_buffers_c = 32;
            static std::string ret_buffers[ret_buffers_c];
            static int ret_pos = 0;
            Internal::ss.clear();
            Internal::ss.flags(Internal::stdfmt);
            Internal::ss.str("");
            int dummy[] {(Internal::ss << p, 0)...};
            (void)dummy;
            ret_buffers[ret_pos] = Internal::ss.str();
            const char *ret = ret_buffers[ret_pos].c_str();
            ret_pos = (ret_pos + 1) % ret_buffers_c;
            return ret;
        }

        inline std::string FixEdges_s(const char *txt) // Removes non-printable chars and spaces from start and end of a string.
        {
            while ((unsigned char)*txt <= ' ' || *txt == 127)
            {
                if (!*txt)
                    return "";
                txt++;
            }

            const char *end = txt;
            while (*end)
                end++;
            do
                end--;
            while ((unsigned char)*end <= ' ' || *end == 127);
            end++;

            return std::string(txt, end);
        }
        inline const char *FixEdges(const char *txt) // Removes non-printable chars and spaces from start and end of a string.
        {
            static std::string ret;

            while ((unsigned char)*txt <= ' ' || *txt == 127)
            {
                if (!*txt)
                {
                    ret = "";
                    return ret.c_str();
                }
                txt++;
            }

            const char *end = txt;
            while (*end)
                end++;
            do
                end--;
            while ((unsigned char)*end <= ' ' || *end == 127);
            end++;


            using std::string; // This is needed for the next line to work.
            ret.~string();
            new(&ret) string(txt, end);

            return ret.c_str();
        }
    }
    using Strings::Jo;
    using Strings::Jo_s;

    inline namespace Classes
    {
        template <typename T> class Array
        {
            static_assert(std::is_const<T>::value == 0, "Mutable arrays of const values are not supported, use const arrays instead.");

            T *data;
            std::size_t size;

          public:
            using type = T;

            void Alloc(std::size_t new_size) // You can call this safely at any time.
            {
                if (data)
                    delete [] data;
                size = new_size;
                data = new T[size];
            }
            void Free() // You can call this safely at any time.
            {
                if (data)
                {
                    delete [] data;
                    data = 0;
                }
                size = 0;
            }

            std::size_t Size() const
            {
                return size;
            }

            std::size_t ByteSize() const
            {
                return size * sizeof (T);
            }

            T &operator*() {return *data;}
            const T &operator*() const {return *data;}
            operator T *() {return data;}
            operator const T *() const {return data;}

            T *begin() {return data;}
            T *Begin() {return data;}
            T *end() {return data+size;}
            T *End() {return data+size;}
            const T *begin() const {return data;}
            const T *Begin() const {return data;}
            const T *end() const {return data+size;}
            const T *End() const {return data+size;}

            Array()
            {
                data = 0; size = 0;
            }
            Array(std::size_t size) : Array()
            {
                Alloc(size);
            }
            Array(std::initializer_list<T> list) : Array()
            {
                Alloc(list.size());
                for (std::size_t i = 0; i < list.size(); i++)
                    (*this)[i] = list.begin()[i];
            }

            Array(const Array &o) : Array()
            {
                Alloc(o.size);
                for (std::size_t i = 0; i < o.size; i++)
                    (*this)[i] = o[i];
            }
            Array(Array &&o)
            {
                data = o.data;
                size = o.size;
                o.data = 0;
                o.size = 0;
            }
            Array &operator=(const Array &o)
            {
                if (&o == this)
                    return *this;
                Alloc(o.size);
                for (std::size_t i = 0; i < o.size; i++)
                    (*this)[i] = o[i];
                return *this;
            }
            Array &operator=(Array &&o)
            {
                if (&o == this)
                    return *this;
                if (data)
                    delete [] data;
                data = o.data;
                size = o.size;
                o.data = 0;
                o.size = 0;
                return *this;
            }

            ~Array()
            {
                if (data)
                    delete [] data;
            }
        };

        template <typename ID = uint32_t, typename Index = ID> class PoolManager
        {
            static_assert(sizeof (ID) >= sizeof (Index), "`sizeof (ID)` must me larger than or equal to `sizeof (Index)`.");
            static_assert(std::is_integral<ID>::value && std::is_integral<Index>::value, "Integral types must be used.");

            Index size, pos;
            Utils::Array<ID> pool;
            Utils::Array<Index> locs;

            PoolManager(const PoolManager &) = delete;
            PoolManager(PoolManager &&) = delete;
            PoolManager &operator=(const PoolManager &) = delete;
            PoolManager &operator=(PoolManager &&) = delete;

          public:
            PoolManager(Index pool_size = 0)
            {
                Resize(pool_size);
            }
            ~PoolManager() {}

            void Resize(Index pool_size)
            {
                if ((unsigned long long)pool_size > ipow<unsigned long long>(2, sizeof (Index) * 8))
                    Sys::Error(Jo("Requested size (", pool_size, ") of a pool manager is larger than the pool type (", sizeof (Index)," bytes, ", ipow<unsigned long long>(2, sizeof (Index) * 8), " possible values) can support."));

                size = pool_size;
                pos = 0;
                pool.Alloc(size);
                locs.Alloc(size);
                for (Index i = 0; i < pool_size; i++)
                    pool[i] = locs[i] = (Index)i;
            }

            bool Alloc(ID *ptr) // Returns 0 on fail.
            {
                if (pos >= size)
                    return 0;
                *ptr = pool[pos++];
                return 1;
            }
            bool Free(ID id) // Returns 0 if such id was not allocated.
            {
                if (locs[id] >= pos)
                    return 0;
                ID last_id = pool[pos-1];
                std::swap(pool[locs[id]], pool[pos-1]);
                std::swap(locs[id], locs[last_id]);
                pos--;
                return 1;
            }
            void AllocEverything()
            {
                pos = size;
            }
            void FreeEverything()
            {
                pos = 0;
            }
            Index MaxSize() const
            {
                return size;
            }
            Index CurrentSize() const
            {
                return pos;
            }
            const ID *Pool() const // First CurrentSize() ids in it are allocated, next (MaxSize()-CurrentSize()) ids in it are free.
            {
                return pool;
            }
            const Index *Indexes() const // Use Indexes()[id] to get a position of id inside of the Pool(). 0 <= id < MaxSize().
            {
                return locs;
            }
        };

        class TickStabilizer
        {
            uint64_t tick_len, begin_time;
            unsigned int tick_limit;
            bool lag_flag;

            TickStabilizer(const TickStabilizer &) = delete;
            TickStabilizer(TickStabilizer &&) = delete;
            TickStabilizer &operator=(const TickStabilizer &) = delete;
            TickStabilizer &operator=(TickStabilizer &&) = delete;
          public:
            TickStabilizer(double freq, unsigned int max_tick_queued = 16)
            {
                SetFreq(freq);
                SetTickLimit(max_tick_queued);
                Reset();
            }

            bool Lag() // Returns 1 if max tick queue len is reached, then the flag is reseted to 0.
            {
                if (lag_flag)
                {
                    lag_flag = 0;
                    return 1;
                }
                return 0;
            }

            void Reset()
            {
                begin_time = Clock::Time();
            }

            void SetFreq(double freq)
            {
                tick_len = Clock::Tps() / freq;
            }

            void SetTickLimit(unsigned int max_tick_queued = 64) // This limits an amout of ticks that would be made after an eternal lag.
            {
                tick_limit = max_tick_queued;
            }

            bool Tick(uint64_t cur_time = Sys::TickTime()) // Use it like this: `while (_.Tick()) {Your tick code}`
            {
                if (cur_time - begin_time > tick_len)
                {
                    if (cur_time - begin_time > tick_len * tick_limit)
                    {
                        begin_time = cur_time - tick_len * tick_limit;
                        lag_flag = 1;
                    }
                    begin_time += tick_len;
                    return 1;
                }
                return 0;
            }
            bool TickNeeded(uint64_t cur_time = Sys::TickTime()) // Only checks if tick is needed without performing it.
            {
                return cur_time - begin_time > tick_len;
            }

            double Time(uint64_t cur_time = Sys::TickTime()) // Returns time since last tick, measured in ticks. Useful for rendering moving things when FPS is higher than tickrate.
            {
                return double(cur_time - begin_time) / tick_len;
            }

            uint64_t TickLen()       {return tick_len;}
            unsigned int TickLimit() {return tick_limit;}
        };

        template <typename T> class ArrayProxy
        {
            T *first;
            std::size_t size;

          public:
            using type = typename std::remove_const<T>::type;
            static constexpr bool readonly = std::is_const<T>::value;

            T &operator*() const {return *first;}
            operator T *() const {return first;}

            T *begin() const {return first;}
            T *Begin() const {return first;}
            T *end() const {return first+size;}
            T *End() const {return first+size;}

            typename std::conditional<readonly, const void *, void *>::type Data() const {return first;}
            std::size_t Size() const {return size;}

            ArrayProxy() // Null.
            {
                first = 0;
                size = 0;
            }
            ArrayProxy(T &ptr) // From a single object.
            {
                first = &ptr;
                size = 1;
            }
            ArrayProxy(T *a, T *b) // From a pair of pointers.
            {
                first = a;
                size = b - a;
            }
            ArrayProxy(T *ptr, std::size_t len) // From a pointer and a size.
            {
                first = ptr;
                size = len;
            }
            template <typename TT> ArrayProxy(TT && o) // From an array or a container which uses pointers as iterators. Use this with caution on temporary containers.
            {
                static_assert(std::is_pointer<decltype(std::begin(o))>::value, "Underlying container must use pointers as iterators or must use contiguous storage.");
                static_assert(readonly || (!std::is_const<typename std::remove_pointer<decltype(std::begin(o))>::type>::value), "Attempt to bind read-write proxy to a const object.");
                first = std::begin(o);
                size = std::end(o) - std::begin(o);
            }
            ArrayProxy(std::initializer_list<type> list) // From std::initializer_list (which is always const). Use this with caution on temporary lists.
            {
                first = list.begin();
                size = list.size();
            }
            template <std::size_t S> ArrayProxy(std::array<type, S> &arr) // From std::array.
            {
                // This function may be redundant on some systems, but this is not guaranteed.
                first = &*arr.begin();
                size = &*arr.end() - &*arr.begin();
            }
            template <std::size_t S> ArrayProxy(const std::array<type, S> &arr) // From const std::array. Use this with caution on temporary arrays.
            {
                // This function may be redundant on some systems, but this is not guaranteed.
                static_assert(readonly, "Attempt to bind read-write proxy to a const object.");
                first = &*arr.begin();
                size = &*arr.end() - &*arr.begin();
            }
            ArrayProxy(std::vector<type> &arr) // From std::vector.
            {
                first = &*arr.begin();
                size = &*arr.end() - &*arr.begin();
            }
            ArrayProxy(const std::vector<type> &arr) // From const std::vector. Use this with caution on temporary vectors.
            {
                static_assert(readonly, "Attempt to bind read-write proxy to a const object.");
                first = &*arr.begin();
                size = &*arr.end() - &*arr.begin();
            }
        };

        template <typename T> using ArrayViewer = ArrayProxy<const T>;
    }
}
using Utils::Jo;
using Utils::Jo_s;

#endif

