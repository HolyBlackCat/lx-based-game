#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

#include <cstdint>
#include <cstring>

#include "exceptions.h"
#include "lib_al.h"
#include "math.h"
#include "system.h"
#include "utils.h"

namespace Audio
{
    #ifdef LXINTERNAL_AUDIO_H_SPECIAL_ACCESS
    void Init();
    void Cleanup();
    void Tick();
    #endif

    void ForceErrorCheck();

    inline void Volume(float vol)
    {
        alListenerf(AL_GAIN, vol);
    }

    inline void Pitch(float pitch)
    {
        alListenerf(AL_PITCH, pitch);
    }

    inline void ListenerPos(const fvec3 &v)
	{
		alListenerfv(AL_POSITION, v.as_array());
	}
	inline void ListenerVelocity(const fvec3 &v)
	{
		alListenerfv(AL_VELOCITY, v.as_array());
	}
	inline void ListenerRot(const fvec3 &forw, const fvec3 &up)
	{
		fvec3 arr[2] = {forw, up};
		alListenerfv(AL_ORIENTATION, (float *)arr);
	}

	inline void DopplerFactor(float n) {alDopplerFactor(n);}
	inline void SpeedOfSound (float n) {alSpeedOfSound(n);}

    enum class SoundFormat
    {
        mono8    = AL_FORMAT_MONO8,
        mono16   = AL_FORMAT_MONO16,
        stereo8  = AL_FORMAT_STEREO8,
        stereo16 = AL_FORMAT_STEREO16,
    };

    class SoundData
    {
        Utils::Array<uint8_t> data;
        uint32_t size; // Amount of samples.
        uint32_t freq;
        SoundFormat format;
      public:
        static unsigned int FormatBytesPerSample(SoundFormat format)
        {
            switch (format)
            {
                case SoundFormat::mono8:    return 1;
                case SoundFormat::mono16:   return 2;
                case SoundFormat::stereo8:  return 2;
                case SoundFormat::stereo16: return 4;
            }
            return 0;
        }

        void LoadWAV(const char *fname); // Can throw CantOpenFileForReading, CantParseFile and UnexpectedEndOfFile.
        void LoadOGG(const char *fname, bool load_as_8bit = 0); // Can throw CantOpenFileForReading, CantParseFile and UnexpectedEndOfFile.
        void SaveWAV(const char *fname); // Can throw CantOpenFileForWriting.
        void LoadCompressed(const char *fname); // Can throw CantOpenFileForReading, CantParseFile and UnexpectedEndOfFile.
        void SaveCompressed(const char *fname); // Can throw CantOpenFileForWriting.
        void LoadWAV_Mono(const char *fname) // Can throw same as Load() plus UnexpectedFileContent.
        {
            LoadWAV(fname);
            if (Stereo())
                throw Exception::UnexpectedFileContent({fname, "Mono sound", "Stereo sound"});
        }
        void LoadCompressed_Mono(const char *fname) // Can throw same as LoadCompressed() plus UnexpectedFileContent.
        {
            LoadCompressed(fname);
            if (Stereo())
                throw Exception::UnexpectedFileContent({fname, "Mono sound", "Stereo sound"});
        }
        void LoadOGG_Mono(const char *fname, bool load_as_8bit = 0) // Can throw same as LoadOGG() plus UnexpectedFileContent.
        {
            LoadOGG(fname, load_as_8bit);
            if (Stereo())
                throw Exception::UnexpectedFileContent({fname, "Mono sound", "Stereo sound"});
        }
        void LoadWAV_Stereo(const char *fname) // Can throw same as Load() plus UnexpectedFileContent.
        {
            LoadWAV(fname);
            if (Mono())
                throw Exception::UnexpectedFileContent({fname, "Stereo sound", "Mono sound"});
        }
        void LoadCompressed_Stereo(const char *fname) // Can throw same as LoadCompressed() plus UnexpectedFileContent.
        {
            LoadCompressed(fname);
            if (Mono())
                throw Exception::UnexpectedFileContent({fname, "Stereo sound", "Mono sound"});
        }
        void LoadOGG_Stereo(const char *fname, bool load_as_8bit = 0) // Can throw same as LoadOGG() plus UnexpectedFileContent.
        {
            LoadOGG(fname, load_as_8bit);
            if (Mono())
                throw Exception::UnexpectedFileContent({fname, "Mono sound", "Stereo sound"});
        }

        static SoundData FromWAV              (const char *fname) {SoundData ret; ret.LoadWAV              (fname); return ret;}
        static SoundData FromCompressed       (const char *fname) {SoundData ret; ret.LoadCompressed       (fname); return ret;}
        static SoundData FromWAV_Mono         (const char *fname) {SoundData ret; ret.LoadWAV_Mono         (fname); return ret;}
        static SoundData FromWAV_Stereo       (const char *fname) {SoundData ret; ret.LoadWAV_Stereo       (fname); return ret;}
        static SoundData FromCompressed_Mono  (const char *fname) {SoundData ret; ret.LoadCompressed_Mono  (fname); return ret;}
        static SoundData FromCompressed_Stereo(const char *fname) {SoundData ret; ret.LoadCompressed_Stereo(fname); return ret;}
        static SoundData FromOGG              (const char *fname, bool load_as_8bit = 0) {SoundData ret; ret.LoadOGG       (fname, load_as_8bit); return ret;}
        static SoundData FromOGG_Mono         (const char *fname, bool load_as_8bit = 0) {SoundData ret; ret.LoadOGG_Mono  (fname, load_as_8bit); return ret;}
        static SoundData FromOGG_Stereo       (const char *fname, bool load_as_8bit = 0) {SoundData ret; ret.LoadOGG_Stereo(fname, load_as_8bit); return ret;}


        void Empty(uint32_t new_size, SoundFormat new_format)
        {
            Clear();
            size = new_size;
            format = new_format;
            data.Alloc(ByteSize());
        }
        void LoadFromMem(uint32_t new_size, SoundFormat new_format, uint8_t *mem)
        {
            Empty(new_size, new_format);
            std::memcpy(data, mem, ByteSize());
        }
        void Clear()
        {
            size = 0;
            data.Free();
        }
        void *Data()
        {
            return data;
        }
        const void *Data() const
        {
            return data;
        }
        uint32_t Size() const // Amount of samples, not bytes.
        {
            return size;
        }
        uint32_t ByteSize() const
        {
            return size * FormatBytesPerSample(format);
        }
        void SetFrequency(uint32_t new_freq)
        {
            freq = new_freq;
        }
        uint32_t Frequency() const
        {
            return freq;
        }
        SoundFormat Format() const
        {
            return format;
        }
        bool Mono() const
        {
            return format == SoundFormat::mono8 || format == SoundFormat::mono16;
        }
        bool Stereo() const
        {
            return format == SoundFormat::stereo8 || format == SoundFormat::stereo16;
        }
        bool Has8BitsPerSample() const
        {
            return format == SoundFormat::mono8 || format == SoundFormat::stereo8;
        }
        bool Has16BitsPerSample() const
        {
            return format == SoundFormat::mono16 || format == SoundFormat::stereo16;
        }
        uint8_t &AtMono8   (uint32_t pos) {return *(uint8_t *)(data + pos);}
        int16_t &AtMono16  (uint32_t pos) {return *(int16_t *)(data + pos*2);}
        u8vec2  &AtStereo8 (uint32_t pos) {return *(u8vec2  *)(data + pos*2);}
        i16vec2 &AtStereo16(uint32_t pos) {return *(i16vec2 *)(data + pos*4);}
        uint8_t AtMono8   (uint32_t pos) const {return *(uint8_t *)(data + pos);}
        int16_t AtMono16  (uint32_t pos) const {return *(int16_t *)(data + pos*2);}
        u8vec2  AtStereo8 (uint32_t pos) const {return *(u8vec2  *)(data + pos*2);}
        i16vec2 AtStereo16(uint32_t pos) const {return *(i16vec2 *)(data + pos*4);}
        SoundData()
        {
            size = 0;
            format = SoundFormat::mono8;
        }
        SoundData(uint32_t new_size, SoundFormat new_format)
        {
            size = new_size;
            format = new_format;
            data.Alloc(ByteSize());
        }
        ~SoundData()
        {
            Clear();
        }
    };

    class Buffer
    {
        ALuint handle;
        bool stereo;
      public:
        void SetData(const SoundData &data)
        {
            stereo = data.Stereo();
            alBufferData(handle, (ALenum)data.Format(), data.Data(), data.ByteSize(), data.Frequency());
        }

        bool Mono()   const {return !stereo;}
        bool Stereo() const {return  stereo;}

        ALuint Handle() const {return handle;}

        Buffer()
        {
            alGenBuffers(1, &handle);
        }
        Buffer(const SoundData &data) : Buffer()
        {
            SetData(data);
        }

        void Play(const fvec3 &pos, float volume = 1, float pitch = 1) const;
        void Play(float volume = 1, float pitch = 1) const;


        ~Buffer()
        {
            alDeleteBuffers(1, &handle);
        }
    };

    enum class SourceMode
    {
        once,             // Plays sound once.       Sound keeps playing if source object is destroyed.
        once_managed,     // Plays sound once.       Sound STOPS playing if source object is destroyed.
        loop,             // Plays sound infinitely. Sound STOPS playing if source object is destroyed.
    };

    class Source
    {
        int id;
        bool stereo;

        Source(const Source &) = delete;
        Source(Source &&) = delete;
        Source &operator=(const Source &) = delete;
        Source &operator=(Source &&) = delete;
      public:

        Source()                                {id = -1;}
        Source(const Buffer &buffer) : Source() {Open(buffer);}

        void Open(const Buffer &buffer);
        void Close();

        bool Mono()   const {return !stereo;}
        bool Stereo() const {return  stereo;}

        void Mode(SourceMode mode);
        void RefDistance(float n);
        void MaxDistance(float n);
        void RolloffPactor(float n);
        void Volume(float n);
        void Pitch(float n);
        void Pos(fvec3 n);
        void Vel(fvec3 n);
        void RelativePosition(bool n);

        bool Playing() const;

        void Play();
        void Stop();

        static int ActiveMono();
        static int ActiveStereo();
        static int MaxMono();
        static int MaxStereo();

        ~Source() {Close();}
    };
}

#endif